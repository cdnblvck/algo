package easy;

public class FindUniqueString {
public class FindUniqueString {
	public char firstUniqChar(String textInput) {
	char error = ' '; // 1
		if (textInput == null || textInput.length() == 0)
			return error; // 1
		char[] splitString = textInput.toCharArray();
		int[] count = new int[256];
		for (int i = 0; i < splitString.length; i++) {
			count[splitString[i]]++;
		}
		for (int i = 0; i < splitString.length; i++) {
			if (count[splitString[i]] == 1)
				return splitString[i];
		}
		return error;
	}
}
}
